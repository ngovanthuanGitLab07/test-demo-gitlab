//Cong hai da thuc.
#include<stdio.h>
typedef struct DATHUC {
	int n;
	int *heso;
};
void nhapdathuc(DATHUC &A){
	printf("Nhap bac cua da thuc :");
	scanf("%d",&A.n);
	A.heso = new int [A.n]; // cap phat bo nho cho mang co so.
	for(int i = 0; i<=A.n; ++i){
		printf("\n Nhap he so thu % d: ",i);
		scanf("%d",&A.heso[i]);
	}
}
void xuatdathuc(DATHUC A){
	printf("%d ",A.heso[0]);
	for(int i = 1; i <=A.n ; ++i){
		if(A.heso[i] >= 0){
			printf("+ %dX^%d ",A.heso[i], i);
		}
	}
}
void Tong(DATHUC A, DATHUC B, DATHUC &C){
	C.n = A.n > B.n ? A.n : B.n;
	C.heso = new int [C.n];
	for(int i = 0; i <= C.n ; ++i){
	if(i<=A.n && i<=B.n)
		C.heso[i] = A.heso[i] + B.heso[i];
	else 
		C.heso[i] = i <= A.n ? A.heso[i] : B.heso[i];
	}
}
main(){
	DATHUC A,B,C;
	printf("\nNHAP DA THUC:\n");
	nhapdathuc(A);
	printf("\nNHAP DA THUC:\n");
	nhapdathuc(B);
	//Xuat hai da thuc
	printf("\nDA THUC:\n");
	xuatdathuc(A);
	printf("\n");
	xuatdathuc(B);
	//Tong hai da thuc.
	Tong(A,B,C);
	printf("\n C(x) = A(X) + B(x):");
	xuatdathuc(C);
}
