#include <stdio.h>
#include <conio.h>
int DemChan(int &n, int &count)
{
	int temp;
	while(n != 0)
	{
		temp = n%10;
		n /= 10;
		if(temp % 2 != 0)
		{
			count++;
			return temp;
		}		
	}
}

int main()
{
	int n = 12345; // tao mot day so
	int count = 0;
	printf("\nCac so le co trong day: ");
	int demchan = DemChan(n, count);
	printf("\t%d", demchan);
	while(n !=0)
	{
		demchan = DemChan(n, count);
		printf("\t%d", demchan);
	}
	printf("\nCo %d do le co trong mang", count);
	
	return 0;
}
