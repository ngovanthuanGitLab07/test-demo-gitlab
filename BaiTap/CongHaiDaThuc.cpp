#include <iostream>
using namespace std;

struct DaThuc
{
	int n; // S? b?c cao nh?t c?a �a th?c.
	float *HeSo; // M?ng danh s�ch c�c h? s?.
};
typedef struct DaThuc DATHUC;

void NhapDaThuc(DATHUC *x)
{
	do{
		cout << "\nNhap vao so bac cua da thuc: ";
		cin >> x ->n;

		if(x ->n < 1)
		{
			cout << "\nSo bac cua da thuc phai >= 1. Xin kiem tra lai !";
		}
	}while(x ->n < 1);
	
	x ->HeSo = new float[x ->n + 1];
	for(int i = 0; i <= x ->n; i++)
	{
		cout << "\nNhap vao he so thu " << i << " la: ";
		cin >> x ->HeSo[i];
	}
}

void XuatDaThuc(DATHUC *x)
{
	if(x ->n == 1)
	{
		cout << x ->HeSo[0] << " + ";
		cout << x ->HeSo[1] << " * x";
	}
	else
	{
		cout << x ->HeSo[0] << " + ";
		cout << x ->HeSo[1] << " * x + ";
		for(int i = 2; i < x ->n; i++)
		{
			cout << x ->HeSo[i] << " * x^" << i << " + ";
		}
		cout << x ->HeSo[x ->n] << " * x^" << x ->n;
	}
}

int main(int argc, char **argv)
{
	DATHUC *x = new DATHUC;

	NhapDaThuc(x);
	XuatDaThuc(x);

	delete x;
	system("pause");
	return 0;
}
