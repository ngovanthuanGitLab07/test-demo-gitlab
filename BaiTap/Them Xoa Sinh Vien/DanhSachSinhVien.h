#pragma once
#include "SinhVien.h"
struct DanhSachSinhVien
{
	int n;
	SINHVIEN *arr;
};
typedef struct DanhSachSinhVien DANHSACHSINHVIEN;
//template <typename sinhvien>
void REALLOC(SINHVIEN *&a, int OldSize, int NewSize)
{
	SINHVIEN *temp = new SINHVIEN[OldSize];

	for(int i = 0; i < OldSize; i++)
	{
		temp[i] = a[i];
	}

	delete[] a;
	a = new SINHVIEN[NewSize];

	int Min = OldSize < NewSize ? OldSize : NewSize;

	for(int i = 0; i < Min; i++)
	{
		a[i] = temp[i];
	}

	delete[] temp;
}

void NhapDanhSachSinhVien(DANHSACHSINHVIEN &ds)
{
		do{
		printf("\nNhap vao so luong sinh vien: ");
		scanf("%d", &ds.n);

		if(ds.n < 0)
		{
			printf("\nSo luong sinh vien phai la so duong. Xin kiem tra lai !");
		}
	}while(ds.n < 0);

	ds.arr = new SINHVIEN[ds.n];

	for(int i = 0; i < ds.n; i++)
	{
		printf("\n-------- Nhap vao sinh vien thu %d --------\n", i + 1);
		NhapSinhVien(ds.arr[i]);
	}
}
void XuatDanhSinhVien(DANHSACHSINHVIEN ds)
{
	for(int i = 0; i < ds.n; i++)
	{
		printf("\n-------- sinh vien thu %d --------\n", i + 1);
		XuatSinhVien(ds.arr[i]);
	}
}
void ThemSinhVien(DANHSACHSINHVIEN &ds, int vitrithem)
{
	REALLOC(ds.arr, ds.n, ds.n + 1);
	for(int i = ds.n - 1; i >=vitrithem; i--)
	{
		ds.arr[i + 1] = ds.arr[i];
	}
	ds.n++;
}
void XoaSinhVien(DANHSACHSINHVIEN &ds, int vitrixoa)
{
	for(int i = vitrixoa + 1; i < ds.n; i++)
	{
		ds.arr[i - 1] = ds.arr[i];
	}
	ds.n--;
	REALLOC(ds.arr, ds.n + 1, ds.n);
}


