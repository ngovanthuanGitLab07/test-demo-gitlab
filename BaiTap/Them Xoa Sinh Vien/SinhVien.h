#include <stdio.h>
#include <conio.h>
#include <string.h>
struct SinhVien
{
	char masv[30],ten[30],lop[10];
	float dtb;
};
typedef struct SinhVien SINHVIEN;

void NhapSinhVien(SINHVIEN &sv)
{
	fflush(stdin);
	printf("\nNhap vao ma sinh vien: ");
	gets(sv.masv);
	printf("\nNhap vao ma ten sinh vien: ");
	gets(sv.ten);
	printf("\nNhap vao lop hoc: ");
	gets(sv.lop);
	do{
		printf("\nNhap vao so diem trung binh: ");
		scanf("%f", &sv.dtb);

		if(sv.dtb < 0 || sv.dtb > 10)
		{
			printf("\nSo diem phai nam trong doan tu 0 --> 10. Xin kiem tra lai !");
		}
	}while(sv.dtb < 0 || sv.dtb > 10);
}
void XuatSinhVien(SINHVIEN sv)
{
	printf("\nMa sinh vien: %s", sv.masv);
	printf("\nTen sinh vien: %s", sv.ten);
	printf("\nLop: %s", sv.lop);
	printf("\ndtb: %f", sv.dtb);
}


